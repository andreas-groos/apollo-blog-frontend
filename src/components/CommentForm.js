import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Row, Col } from 'reactstrap';

const CommentForm = props => {
  const { handleSubmit, pristine, reset, submitting } = props;

  if (!props.user.user) {
    return (
      <p>
        <small>You must be logged in to add comments</small>
      </p>
    );
  } else {
    return (
      <div>
        <form onSubmit={handleSubmit}>
          <Row>
            <Col>
              <div className="form-group">
                <div>
                  <Field
                    name="comment"
                    placeholder="your comment here"
                    component="textarea"
                    rows="3"
                    className="form-control"
                  />
                </div>
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <div className="d-flex justify-content-center">
                <button
                  className="btn btn-primary"
                  type="submit"
                  disabled={pristine || submitting}
                >
                  Submit
                </button>
                <button
                  className="btn btn-primary"
                  type="button"
                  disabled={pristine || submitting}
                  onClick={reset}
                >
                  Cancel
                </button>
              </div>
            </Col>
          </Row>
        </form>
      </div>
    );
  }
};

export default reduxForm({ form: 'newComment' })(CommentForm);
