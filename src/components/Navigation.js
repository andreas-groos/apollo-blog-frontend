import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import firebase, { auth, providerGoogle } from '../constants/firebaseConfig';
import { NavLink } from 'react-router-dom';
import gql from 'graphql-tag';
import { ADD_USER } from '../apollo/mutations';
import client from '../apollo/apolloClient';

import {
  Row,
  Nav,
  Collapse,
  Button,
  Navbar,
  NavbarToggler,
  NavItem,
  Container,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle
} from 'reactstrap';
import * as authActions from '../actions/authActions';

class Navigation extends Component {
  static propTypes = {
    // prop: PropTypes
  };

  constructor(props) {
    super(props);

    this.state = {
      collapsedNavbar: true,
      dropdownOpen: false,
      user: null
    };
  }
  componentDidMount = () => {
    auth.onAuthStateChanged(user => {
      if (user) {
        this.setState({ user });
        this.props.login(user);
      } else {
        this.props.logout();
      }
    });
  };

  logout = () => {
    auth.signOut().then(() => {
      this.setState({ user: null });
    });
    this.props.logout();
  };

  login = () => {
    auth.signInWithPopup(providerGoogle).then(result => {
      const user = result.user;
      if (user.metadata.creationTime === user.metadata.lastSignInTime) {
        // new user
        //  TODO: Still adds user to firebase even if my own validation figures 'email/name already exists'
        console.log('user', user);
        client.mutate({
          mutation: ADD_USER,
          variables: {
            name: user.displayName,
            email: user.email,
            uid: user.uid
          }
        });
      }
      this.setState({ user });
      this.props.login(user);
    });
  };

  toggleNavbar = () => {
    this.setState({
      collapsedNavbar: !this.state.collapsedNavbar
    });
  };

  dropdownToggle = () => {
    this.setState({ dropdownOpen: !this.state.dropdownOpen });
  };

  render() {
    return (
      <div>
        <Container fluid>
          <Row>
            <Navbar
              id="navbar"
              color="primary"
              dark
              className="w-100 "
              expand="md"
            >
              {/* <NavbarBrand href="/" className="mr-auto">
                reactstrap
              </NavbarBrand> */}
              <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
              <Collapse
                isOpen={!this.state.collapsedNavbar}
                navbar
                className="d-lg-flex justify-content-between"
              >
                <Nav navbar>
                  <NavItem>
                    <NavLink
                      className="nav-link"
                      activeClassName="active"
                      exact={true}
                      to="/"
                    >
                      Home
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className="nav-link"
                      activeClassName="active"
                      to="/new_post"
                    >
                      New Post
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className="nav-link"
                      activeClassName="active"
                      to="/about"
                    >
                      About
                    </NavLink>
                  </NavItem>
                </Nav>
                {this.state.user ? (
                  <Dropdown
                    isOpen={this.state.dropdownOpen}
                    toggle={this.dropdownToggle}
                  >
                    <DropdownToggle outline color="warning" caret>
                      {this.state.user.displayName}
                      <img
                        src={this.state.user.photoURL}
                        height="30px"
                        className="mx-2"
                      />
                    </DropdownToggle>
                    <DropdownMenu>
                      <DropdownItem onClick={this.logout}>Logout</DropdownItem>
                      <DropdownItem>Settings</DropdownItem>
                    </DropdownMenu>
                  </Dropdown>
                ) : (
                  <Button color="light" onClick={this.login}>
                    Login
                  </Button>
                )}
              </Collapse>
            </Navbar>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

const mapDispatchToProps = dispatch => ({
  login: user => dispatch(authActions.loginAction(user)),
  logout: () => dispatch(authActions.logoutAction())
});

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
