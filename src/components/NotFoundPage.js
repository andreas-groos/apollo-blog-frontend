import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faLink from '@fortawesome/fontawesome-free-solid/faLink';

const NotFoundPage = () => {
  return (
    <Container>
      <Row>
        <Col>
          <h1 className="text-center my-2">404 not found</h1>
          <Link to="/">
            <h4 className="text-center">Go back to homepage</h4>
          </Link>
          <hr />
          <FontAwesomeIcon
            style={{ margin: 'auto', display: 'block' }}
            icon={faLink}
            size="6x"
          />
        </Col>
      </Row>
    </Container>
  );
};

export default NotFoundPage;
