import React, { Component } from 'react';
import { connect } from 'react-redux';
import CommentForm from './CommentForm';
import SingleComment from './SingleComment';
import { Row, Col } from 'reactstrap';
import { Mutation } from 'react-apollo';
import { ADD_COMMENT } from '../apollo/mutations';
import { GET_POST } from '../apollo/queries';
import { reset } from 'redux-form';

class CommentSection extends Component {
  render() {
    return (
      <div>
        {this.props.comments.map((c, index) => {
          return <SingleComment key={index} comment={c} />;
        })}
        <Mutation mutation={ADD_COMMENT}>
          {(addComment, { data, loading, error }) => (
            <Row>
              <Col>
                <div className="border">
                  <CommentForm
                    user={this.props.user}
                    onSubmit={() => {
                      this.props.reset(); // resets form!
                      addComment({
                        variables: {
                          text: this.props.form.newComment.values.comment,
                          user: this.props.user.user.displayName,
                          id: this.props.id
                        },
                        refetchQueries: [
                          {
                            query: GET_POST,
                            variables: {
                              id: this.props.id
                            }
                          }
                        ]
                      });
                    }}
                  />
                </div>
              </Col>
            </Row>
          )}
        </Mutation>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  reset: () => dispatch(reset('newComment'))
});

const mapStateToProps = state => ({
  form: state.form,
  user: state.user
});
export default connect(mapStateToProps, mapDispatchToProps)(CommentSection);
