import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import PostList from './PostList';

export default class HomePage extends Component {
  render() {
    return (
      <Container>
        <Row>
          <Col>
            <PostList history={this.props.history} />
          </Col>
        </Row>
      </Container>
    );
  }
}
