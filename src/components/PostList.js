import React, { Component } from 'react';
import { Query, Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { GET_AUTHORS, GET_POSTS } from '../apollo/queries';
import { distanceInWordsToNow } from 'date-fns';
import PostExcerpt from './PostExcerpt';

class PostList extends Component {
  render() {
    return (
      <div>
        {/* <Query query={GET_AUTHORS}>
          {({ loading, error, data }) => {
            if (error) return <p>Error</p>;
            {
              console.log('error', error);
              console.log('loading', loading);
              console.log('data authors', data.authors);
            }
            if (loading || !data) return <p>Fetching</p>;

            return data.authors.map(a => {
              return <h1>{a.name}</h1>;
            });
          }}
        </Query> */}
        <Query query={GET_POSTS}>
          {({ loading, error, data }) => {
            if (error) return <p>Error</p>;
            {
              console.log('error', error);
              console.log('loading', loading);
              console.log('data', data);
            }
            if (loading || !data) return <p>Fetching</p>;

            return data.posts.map((p, index) => (
              <div
                key={index}
                onClick={() => {
                  console.log(p.id);
                  this.props.history.push(`/post/${p.id}`);
                }}
              >
                <h4 className="text-center">{p.title}</h4>
                <div className="d-flex justify-content-between">
                  <p>
                    <small>{p.authorName}</small>
                  </p>
                  <p>
                    <small>{p.likes} likes</small>
                  </p>
                  <p>
                    <small>{distanceInWordsToNow(p.createdAt)} ago</small>
                  </p>
                </div>
                <PostExcerpt body={p.blogText} />
                <hr />
              </div>
            ));
          }}
        </Query>
      </div>
    );
  }
}

export default PostList;
