import React from 'react';
import { Container, Row, Col, Jumbotron } from 'reactstrap';

export default () => {
  return (
    <Container>
      <Row>
        <Col>
          <Jumbotron className="mt-2 bg-primary text-white">
            <h1>About:</h1>
            <hr />
            <h3>More information coming soon....</h3>
          </Jumbotron>
        </Col>
      </Row>
    </Container>
  );
};
