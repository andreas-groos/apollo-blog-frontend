import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Query, Mutation } from 'react-apollo';
import { GET_POST, GET_POSTS } from '../apollo/queries';
import { ADD_LIKE } from '../apollo/mutations';
import { Container, Row, Col } from 'reactstrap';
import CommentSection from './CommentSection';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faThumbsUp from '@fortawesome/fontawesome-free-solid/faThumbsUp';
import { format } from 'date-fns';
import Markdown from 'react-markdown';

class Post extends Component {
  componentDidMount = () => {
    let { match: { params } } = this.props;
    console.log('params', params);
  };

  render() {
    return (
      <Container>
        <Row>
          <Col>
            <Query
              query={GET_POST}
              variables={{ id: this.props.match.params.id }}
            >
              {({ loading, error, data }) => {
                console.log('error', error);
                if (error) return <p>{error.graphQLErrors[0].data.message}</p>;
                if (loading) return <p>Loading</p>;
                console.log('data', data);
                let {
                  title,
                  blogText,
                  authorName,
                  createdAt,
                  likes,
                  comments,
                  id,
                  likesBy
                } = data.post;
                return (
                  <div>
                    <h4>{title}</h4>
                    <p className="lead">Author: {authorName}</p>
                    <Markdown source={blogText} />
                    <p>{likes} likes</p>
                    {this.props.user.user ? (
                      <Mutation mutation={ADD_LIKE}>
                        {(addLike, { loading, error }) => {
                          console.log('data', data);
                          if (error) {
                            let customError =
                              error.graphQLErrors[0].data.message;
                            return <p>{customError}</p>;
                          } else {
                            if (!likesBy.includes(this.props.user.user.uid)) {
                              return (
                                <div>
                                  <FontAwesomeIcon
                                    icon={faThumbsUp}
                                    size="lg"
                                    onClick={() => {
                                      addLike({
                                        variables: {
                                          id,
                                          user: this.props.user.user.uid
                                        },
                                        refetchQueries: [
                                          {
                                            query: GET_POST,
                                            variables: {
                                              id: this.props.match.params.id
                                            }
                                          }
                                        ]
                                      }).catch(err => {
                                        console.log('err', err);
                                      });
                                    }}
                                  />
                                </div>
                              );
                            } else {
                              return (
                                <p>
                                  <small>You already liked this post</small>
                                </p>
                              );
                            }
                          }
                        }}
                      </Mutation>
                    ) : (
                      <p>
                        <small>You need to be logged in to 'like' a post</small>
                      </p>
                    )}
                    <CommentSection
                      comments={comments}
                      user={this.props.user}
                      id={id}
                    />
                  </div>
                );
              }}
            </Query>
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

export default connect(mapStateToProps)(Post);
