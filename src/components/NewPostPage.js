import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Query, Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { ADD_POST } from '../apollo/mutations';
import { GET_POSTS } from '../apollo/queries';
import { Container, Row, Col } from 'reactstrap';
import NewPostForm from './NewPostForm';

class NewPostPage extends Component {
  state = {
    submitted: false
  };
  submit = values => {
    console.log(values);
    if (values.postTitle && values.postBody) {
      this.setState({
        submitted: true,
        postTitle: values.postTitle,
        postBody: values.postBody
      });
    }
  };
  render() {
    let postBody = '';

    try {
      postBody = this.props.form.newPost.values.postBody;
    } catch (err) {}

    return (
      <Mutation mutation={ADD_POST}>
        {(addPost, { data }) => (
          <Container>
            <Row>
              <Col>
                <h4 className="text-center">New Post</h4>
              </Col>
            </Row>
            <Row>
              <Col>
                {this.state.submitted ? (
                  <h4>Submitted</h4>
                ) : (
                  <NewPostForm
                    user={this.props.user.user}
                    onSubmit={() => {
                      addPost({
                        variables: {
                          authorName: this.props.user.user.displayName,
                          title: this.props.form.newPost.values.postTitle,
                          blogText: this.props.form.newPost.values.postBody
                        },
                        refetchQueries: ['getPosts']
                      });
                      this.submit(this.props.form.newPost.values);
                    }}
                    postBody={postBody}
                  />
                )}
              </Col>
            </Row>
          </Container>
        )}
      </Mutation>
    );
  }
}

const mapStateToProps = state => ({
  form: state.form,
  user: state.user
});

export default connect(mapStateToProps)(NewPostPage);
