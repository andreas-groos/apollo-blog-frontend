import React from 'react';
import Markdown from 'react-markdown';

export default props => {
  const numnberOfWords = 50;
  let excerpt = '';
  let arr = props.body.split(' ').slice(0, numnberOfWords);
  arr.map(a => (excerpt += a + ' '));
  return (
    <div>
      <Markdown source={excerpt} />
    </div>
  );
};
