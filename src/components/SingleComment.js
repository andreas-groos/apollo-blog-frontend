import React from 'react';
import { format } from 'date-fns';
import { distanceInWordsToNow } from 'date-fns';
import Markdown from 'react-markdown';

import { Row, Col } from 'reactstrap';
export default props => {
  let { user, date, text } = props.comment;
  return (
    <div className="comment">
      <Row>
        <Col>
          <div className="d-flex justify-content-between">
            <p>
              <small>{user}</small>
            </p>
            <p>
              <small>{distanceInWordsToNow(date)} ago</small>
            </p>
          </div>
          <Markdown source={text} />
        </Col>
      </Row>
    </div>
  );
};
