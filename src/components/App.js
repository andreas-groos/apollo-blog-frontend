import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
import HomePage from './HomePage';
import AboutPage from './AboutPage';
import NewPostPage from './NewPostPage';
import Navigation from './Navigation';
import Post from './Post';
import NotFoundPage from './NotFoundPage';
import { ApolloProvider } from 'react-apollo';
import client from '../apollo/apolloClient';

// This is a class-based component because the current
// version of hot reloading won't hot reload a stateless
// component at the top-level.

class App extends React.Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <div>
          <div>
            <Navigation />
          </div>
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/new_post" component={NewPostPage} />
            <Route path="/post/:id" component={Post} />
            <Route path="/about" component={AboutPage} />
            <Route component={NotFoundPage} />
          </Switch>
        </div>
      </ApolloProvider>
    );
  }
}

App.propTypes = {
  children: PropTypes.element
};

export default App;
