import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Row, Col } from 'reactstrap';
import Markdown from 'react-markdown';

const NewPostForm = props => {
  const { handleSubmit, pristine, reset, submitting } = props;
  if (props.user) {
    return (
      <form onSubmit={handleSubmit}>
        <Row>
          <Col>
            <div className="form-group">
              {/* <label>First Name</label> */}
              <div>
                <Field
                  name="postTitle"
                  component="input"
                  type="text"
                  placeholder="Post Title"
                  className="form-control"
                />
              </div>
            </div>
          </Col>
        </Row>
        <Row>
          <Col>
            <p className="small text-center">
              Blogpost can be styled with{' '}
              <strong>github flavord Markdown</strong>
            </p>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <div className="form-group">
              {/* <label className="text-center">Blog body:</label> */}
              <div>
                <Field
                  name="postBody"
                  placeholder="Blog body"
                  component="textarea"
                  rows="20"
                  className="form-control"
                />
              </div>
            </div>
          </Col>
          <Col md={6}>
            <Markdown source={props.postBody} />
          </Col>
        </Row>
        <Row>
          <Col>
            <div className="form-check">
              <Field
                className="form-check-input"
                name="allowComments"
                id="allowComments"
                component="input"
                type="checkbox"
              />
              <label className="form-check-label" htmlFor="allowComments">
                Allow Comments ?
              </label>
            </div>
          </Col>
        </Row>
        <Col md={{ size: 6, offset: 3 }}>
          <div className="d-flex justify-content-between">
            <button
              className="btn btn-primary"
              type="submit"
              disabled={pristine || submitting}
            >
              Submit
            </button>
            <button
              className="btn btn-primary"
              type="button"
              // disabled={pristine || submitting}
              onClick={reset}
            >
              Cancel
            </button>
          </div>
        </Col>
      </form>
    );
  } else {
    return (
      <div>
        <h4 className="text-center">You need to be logged in to post!</h4>;
      </div>
    );
  }
};

export default reduxForm({
  form: 'newPost' // a unique identifier for this form
})(NewPostForm);
