import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as testActions from '../actions/testAction';

export class TestContainer extends Component {
  static propTypes = {
    // prop: PropTypes
  };
  componentDidMount = () => {
    this.props.test({ payload: 'GoGoGo' });
  };

  render() {
    return (
      <div>
        <h1>TestContainer</h1>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  data: state.testData
});

const mapDispatchToProps = dispatch => ({
  test: input => dispatch(testActions.testAction(input))
});

export default connect(mapStateToProps, mapDispatchToProps)(TestContainer);
