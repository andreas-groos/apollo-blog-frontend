import { initialUserState } from './initialUserState';
import { LOG_IN, LOG_OUT } from '../constants/actionTypes';

export default (state = initialUserState, action) => {
  switch (action.type) {
    case LOG_IN:
      let {
        displayName,
        email,
        emailVerified,
        phoneNumber,
        photoURL,
        uid
      } = action.user;
      return Object.assign({}, state, {
        user: {
          displayName,
          email,
          emailVerified,
          phoneNumber,
          photoURL,
          uid
        }
      });
    case LOG_OUT:
      return { ...state, user: null };
    default:
      return state;
  }
};
