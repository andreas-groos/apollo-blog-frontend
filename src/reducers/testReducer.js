import { TEST_ACTION } from "../constants/actionTypes";
import { initialTestState } from "./initialTestState";

export default (state = initialTestState, action) => {
  switch (action.type) {
    case TEST_ACTION:
      return { ...state, msg: action.payload };
    default:
      return state;
  }
};
