// TODO: Update with a new config!
// Initialize Firebase
// FIXME: Check if I do need to import *? Seems to much
import * as firebase from 'firebase';

const firebaseConfig = {
  apiKey: 'AIzaSyBkTk1RPt-Gx0VWyVOg38uV1JR0WN1G65E',
  authDomain: 'react-redux-boilerplate-ab859.firebaseapp.com',
  databaseURL: 'https://react-redux-boilerplate-ab859.firebaseio.com',
  projectId: 'react-redux-boilerplate-ab859',
  storageBucket: 'react-redux-boilerplate-ab859.appspot.com',
  messagingSenderId: '687244813747'
};

firebase.initializeApp(firebaseConfig);

export const providerGoogle = new firebase.auth.GoogleAuthProvider();
// export const providerEmail = new firebase.auth.EmailAuthProvider();
export const auth = firebase.auth();
export default firebase;
