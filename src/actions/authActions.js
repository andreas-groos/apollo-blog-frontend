import * as types from '../constants/actionTypes';

export const loginAction = user => ({
  type: types.LOG_IN,
  user
});

export const logoutAction = () => ({
  type: types.LOG_OUT,
  user: null
});
