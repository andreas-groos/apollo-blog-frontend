import * as types from "../constants/actionTypes";

export const testAction = param => ({
  type: types.TEST_ACTION,
  payload: param.payload
});
